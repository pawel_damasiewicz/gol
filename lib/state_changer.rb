class StateChanger < Struct.new(:board)
  def initialize (*)
    super
    board.display_population
  end

  def next_generation
    all_neighbours
    new_population
  end
  
  private
  def new_population
    board.population.each_with_index do |row, col_index|
      row.each_with_index do |cell, row_index|
        if cell.alive?
          if cell.neighbours < 2
            cell.alive!
          elsif cell.neighbours > 3
            cell.alive!
          end
        else
          if cell.neighbours == 3
            cell.alive!
          end
        end
        cell.clear_neighbours!
      end
    end
  end

  def all_neighbours
    board.population.each_with_index do |row, col_index|
      row.each_with_index do |cell, row_index|
        check_neighbours col_index, row_index, cell
      end
    end
  end

  def check_neighbours col_index, row_index, cell
    #puts
    (-1..1).each do |delt_x|
      (-1..1).each do |delt_y|
        if !((delt_x == 0) && (delt_y == 0))
          #print "#{delt_x}:#{delt_y}."
          board.population[(col_index+delt_x)%board.size][(row_index+delt_y)%board.size].alive? ? cell.add_neighbour! : nil
        end
      end
      #print "\t"
    end
  end
end
