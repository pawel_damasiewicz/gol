require 'require_all'
require_all 'lib'

class Board < Struct.new(:size, :population) 
  include Displayer

  def initialize (*)
    super
    self.size ||= 10
    populate
  end

  def set_cell! x, y
    self.population[x][y].alive!
  end

  def put_pattern! pattern, y_pos, x_pos
    pattern.each_with_index do |row, row_index|
      row.each_with_index do |cell, col_index |
        if cell == 1
          set_cell! row_index+y_pos, col_index+x_pos
        end
      end
    end
  end

private
  def insert_cell x, y
    self.population[x][y] = Cell.new
  end    

  def create_cells
    (0..size-1).each do |col_index|
      (0..size-1).each do |row_index| 
        insert_cell(col_index, row_index)
      end
    end
  end

  def populate
    self.population = Array.new(size)
    self.population.each_with_index do |col, col_index|
      self.population[col_index] = Array.new(size)
    end
    create_cells
  end
end
