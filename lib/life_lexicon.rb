module LifeLexicon
  class String < String
    def convert
      pattern_rows_str = self.split("\n")
      pattern = []
      pattern_rows_str.each_with_index do |row, index|
        pattern[index] = row.chars
        pattern[index].each_with_index do |cell, col_index|
          if cell == 'O'
            pattern[index][col_index] = 1
          else
            pattern[index][col_index] = 0
          end
        end
      end
      pattern
    end
  end
end

