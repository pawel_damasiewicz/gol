module Displayer
  def display_population
    result = ''
    population.each_with_index do |col, col_index|
      population[col_index].each_with_index do |cell, row_index|
        if population[row_index][col_index].alive?
          result += 'o '
        else
          result += '  '
        end
      end
      result.sub!(/\ +\Z/, "")
      result += "\n"
    end
    puts result
  end

  def display_neighbours population
    result = ''
    population.each_with_index do |col, col_index|
      population[col_index].each_with_index do |cell, row_index|
        result += "#{population[row_index][col_index].neighbours} "
      end
      result += "\n"
    end
    puts result
  end
end

