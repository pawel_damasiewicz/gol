class Cell
  attr_accessor :neighbours
  def initialize
    @alive = false
    @neighbours = 0
  end

  def alive?
    @alive
  end

  def alive!
    @alive = alive? ? false : true
  end

  def add_neighbour!
    @neighbours += 1
  end

  def clear_neighbours!
    self.neighbours = 0
  end
end
