require 'require_all'
require_all 'lib'


include LifeLexicon

dinner_table_str = LifeLexicon::String.new(
".O...........
.OOO.......OO
....O......O.
...OO....O.O.
.........OO..
.............
.....OOO.....
.....OOO.....
..OO.........
.O.O....OO...
.O......O....
OO.......OOO.
...........O.")

dragon_str = LifeLexicon::String.new(
".............O..OO......O..OOO
.....O...OOOO.OOOOOO....O..OOO
.OOOOO....O....O....OOO.......
O......OO.O......OO.OOO..O.OOO
.OOOOO.OOO........OOOO...O.OOO
.....O..O..............O......
........OO..........OO.OO.....
........OO..........OO.OO.....
.....O..O..............O......
.OOOOO.OOO........OOOO...O.OOO
O......OO.O......OO.OOO..O.OOO
.OOOOO....O....O....OOO.......
.....O...OOOO.OOOOOO....O..OOO
.............O..OO......O..OOO")


input = [[ 0, 1, 1 ],
         [ 0, 1, 1 ],
         [ 0, 0, 0 ]
]

board = Board.new(45)

#dinner_table = dinner_table_str.convert
dragon = dragon_str.convert

board.put_pattern! input, 40, 40
#put_pattern board, dinner_table, 4, 4
board.put_pattern! dragon, 4, 4

state_changer = StateChanger.new(board)
system('clear')
while(true)
  board.display_population
  state_changer.next_generation
  system('sleep 0.02s;clear')
end

