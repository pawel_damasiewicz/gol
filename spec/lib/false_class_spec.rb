require 'spec_helper'

describe FalseClass do
  describe '#to_i' do
    it { expect(false.to_i).to eq 0 }
  end
end
