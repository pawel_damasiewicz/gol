require 'spec_helper'

describe Cell do
  context 'constructor' do
    let!(:cell){ Cell.new }

    it 'does create dead cell' do 
      expect(cell.alive?).to eq false 
    end

    it 'does set cell neighbours_count to 0' do 
      expect(cell.neighbours).to eq 0
    end
  end
end
