require 'spec_helper'

describe Board do
  describe '#new' do
    let!(:board) { Board.new }

    it { expect(board.class).to eq Board }
    describe '@size' do 
      it { expect(board.size).to eq 10 }
    end

    describe '@population' do
      let!(:population) { Board.new().population }

      it { expect(population.class).to eq Array }

      context 'without size given' do
        let!(:board_without_size) { Board.new().population }

        it { expect(board_without_size.length).to eq 10 }
      end

      context 'with size given' do
        let!(:board_with_size) { Board.new(10).population }

        it { expect(board_with_size.length).to eq 10 }
      end

      describe '[0][0]' do
        it { expect(population[0][0].class).to eq Cell }
      end

    end
  end

  describe '#set_cell' do
    
  end
end
