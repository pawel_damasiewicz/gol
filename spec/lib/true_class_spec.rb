require 'spec_helper'

describe TrueClass do
  describe '#to_i' do
    it { expect(true.to_i).to eq 1 }
  end
end
